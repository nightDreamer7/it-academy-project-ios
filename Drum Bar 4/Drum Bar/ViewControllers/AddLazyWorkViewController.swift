//
//  AddLazyWorkViewController.swift
//  Drum Bar
//
//  Created by Alex on 1/3/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class AddLazyWorkViewController: UIViewController {

    // MARK: Outlets
    
    
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!

//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(with:)), name:  UIResponder.keyboardWillShowNotification, object: nil)
//
//    }
//    @objc func keyboardWillShow(with notification: Notification) {
//        let key = "UIKeyboardFrameEndUserInfoKey"
//        guard let keyboardFrame = notification.userInfo?[key] as? NSValue else {return}
//
//    let keyboardHeight = keyboardFrame.cgRectValue.height + 16
//
//        bottomConstraint?.constant = keyboardHeight
//
//        UIView.animate(withDuration: 0.3, animations: <#T##() -> Void#>)
//        
//        self.view.layoutIfNeeded()
//    
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func done(_ sender: UIButton) {
    }
    
}
