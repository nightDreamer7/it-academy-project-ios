//
//  WeatherViewController.swift
//  Drum Bar
//
//  Created by Alex on 1/3/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
    }
    @IBAction func goToLazyDealApp(_ sender: UIButton) {
    }
    @IBAction func DismissButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension WeatherViewController: UISearchBarDelegate {  // open-closed SOLID
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        let urlString = "https://api.apixu.com/v1/current.json?key=5db0b08c32ed466cb53193644182211&q=\(searchBar.text!.replacingOccurrences(of: " ", with: "%20"))"  // search bar text will be added to our urlString and will be used in our urlSession
        
        var locationName : String?  // parsing json, also we can make it as a struct where we will save our cities and temperature
        var temperature: Double?
        var errorHasOccured: Bool = false  // we are adding our error, if it will be occured , we will change our boolean flag on opposite num
        guard  let url = URL(string: urlString) else { return } // now we are checking our URL
        let task = URLSession.shared.dataTask(with: url) {[weak self] ( data, response , error) in // we must realize our try block throw do-catch block and we are creating our data task and we may get data ,responce or error and we are making it weak
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: AnyObject] // now we are decoding our data with singletone serialization, because data can be in binary format( as 1 0 ) and we will have it in dictionaty  [String: AnyObject]
                
                if let _ = json["error"] {
                    errorHasOccured = true// if we will get error we we are changing our flag on opposite meaning
                }
                if let location = json["location"]  {
                    locationName = location["name"] as? String // parsing json
                    
                }
                
                if let current = json["current"] {// -||-
                    temperature = current["temp_c"] as? Double
                }
                
                DispatchQueue.main.async {// treaths queue: async
                    if errorHasOccured {
                        self?.cityLabel.text = " Error has occured"// results showing
                        self?.temperatureLabel.isHidden = true// flag
                    } else{
                        self?.cityLabel.text = locationName
                        self?.temperatureLabel.text = "\(temperature!)"// results showing
                        
                        self?.temperatureLabel.isHidden = false// flag
                    }
                }
                
                
            }
            catch let jasonError {
                print(jasonError) // we can catch error
            }
            
        }
        task.resume() // if we are working with data task : func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask
//        Discussion
//
//        After you create the task, you must start it by calling its resume() method.!!!
    }
}

