//
//  ViewController.swift
//  SoundTRY
//
//  Created by Alex on 12/27/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import AVFoundation


class DrumViewController: UIViewController {

    
    // Players intitiating
    // There is one player for each button, we have a few problems : when we starting to ply the sound from one burron, we should stop playin sound ftom another buttons and we have spagetti code wif a huge of different constants
    var musicFirstPlayer: AVAudioPlayer = AVAudioPlayer()
    var examplePlayer: AVAudioPlayer = AVAudioPlayer()
    var musicReplayPlayer: AVAudioPlayer = AVAudioPlayer()
    var firstKickPlayer: AVAudioPlayer = AVAudioPlayer()
    var secondKickPlayer: AVAudioPlayer = AVAudioPlayer()
    var thirdKickPlayer: AVAudioPlayer = AVAudioPlayer()
    var fourthKickPlayer: AVAudioPlayer = AVAudioPlayer()
    var fifthsKickPlayer: AVAudioPlayer = AVAudioPlayer()
    var hitPlayer: AVAudioPlayer = AVAudioPlayer()
    var hihatsPlayer: AVAudioPlayer = AVAudioPlayer()
    var fillPlayer: AVAudioPlayer = AVAudioPlayer()
    var firstVoxPlayer: AVAudioPlayer = AVAudioPlayer()
    var secondVoxPlayer: AVAudioPlayer = AVAudioPlayer()
    var firstSamplePlayer: AVAudioPlayer = AVAudioPlayer()
    var secondSamplePlayer: AVAudioPlayer = AVAudioPlayer()
    var firstSnarePlayer: AVAudioPlayer = AVAudioPlayer()
    var secondSnarePlayer: AVAudioPlayer = AVAudioPlayer()
    
    
    var resourse: String? = "melody"
    var type: String? = "mp3"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
// playMusic Button
       let firstMusicFile = Bundle.main.path(forResource: resourse, ofType: type)
        
        do{
            try musicFirstPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: firstMusicFile!))
        }
        
        catch {
            print(error)
        }
        // exampleButton
        let exampleMusicFile = Bundle.main.path(forResource: "lambo1", ofType: "wav")

        do{
            try examplePlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: exampleMusicFile!))
        }

        catch {
            print(error)
        }
        // replayMusicButton
        let secondMusicFile = Bundle.main.path(forResource: "melody", ofType: type)
        
        do{
            try musicReplayPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: secondMusicFile!))
        }
            
        catch {
            print(error)
        }
        // firstKickButton
        let firstKickMusicFile = Bundle.main.path(forResource: "sample kick", ofType: type)
        
        do{
            try firstKickPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: firstKickMusicFile!))
        }
            
        catch {
            print(error)
        }
        //secondKickButton
        let secondKickMusicFile = Bundle.main.path(forResource: "sample kick1", ofType: type)
        
        do{
            try secondKickPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: secondKickMusicFile!))
        }
            
        catch {
            print(error)
        }
        //thirdKickButton
        let thirdKickMusicFile = Bundle.main.path(forResource: "sample kick2", ofType: type)
        
        do{
            try thirdKickPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: thirdKickMusicFile!))
        }
            
        catch {
            print(error)
        }
        //fourthKickButton
        let fourthsKickMusicFile = Bundle.main.path(forResource: "sample kick3", ofType: type)
        
        do{
            try fourthKickPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: fourthsKickMusicFile!))
        }
            
        catch {
            print(error)
        }
        //fourthKickButton
        let fifthsKickMusicFile = Bundle.main.path(forResource: "sample kick4", ofType: type)
        
        do{
            try fifthsKickPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: fifthsKickMusicFile!))
        }
            
        catch {
            print(error)
        }
        //hitButton
        let hitMusicFile = Bundle.main.path(forResource: "sample hit", ofType: type)
        
        do{
            try hitPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: hitMusicFile!))
        }
            
        catch {
            print(error)
        }
        //hihatsButton
        let hihatsMusicFile = Bundle.main.path(forResource: "sample hihats", ofType: type)
        
        do{
            try hihatsPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: hihatsMusicFile!))
        }
            
        catch {
            print(error)
        }
        //fillButton
        let fillMusicFile = Bundle.main.path(forResource: "sample fill", ofType: type)
        
        do{
            try fillPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: fillMusicFile!))
        }
            
        catch {
            print(error)
        }
        //firstVox
        let firstVoxMusicFile = Bundle.main.path(forResource: "sample vox1", ofType: type)
        
        do{
            try firstVoxPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: firstVoxMusicFile!))
        }
            
        catch {
            print(error)
        }
        //secondVox
        let secondVoxMusicFile = Bundle.main.path(forResource: "sample vox2", ofType: "wav")
        
        do{
            try secondVoxPlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: secondVoxMusicFile!))
        }
            
        catch {
            print(error)
        }
        // firstSampleButton
        let firstSampleMusicFile = Bundle.main.path(forResource: "sample pack 2-1acc", ofType: type)
        
        do{
            try firstSamplePlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: firstSampleMusicFile!))
        }
            
        catch {
            print(error)
        }
        // secondSampleButton
        let secondSampleMusicFile = Bundle.main.path(forResource: "sample pack 2-2acc", ofType: type)
        
        do{
            try secondSamplePlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: secondSampleMusicFile!))
        }
            
        catch {
            print(error)
        }
        // firstSnareButton
        let firstSnareMusicFile = Bundle.main.path(forResource: "sample snare", ofType: type)
        
        do{
            try firstSnarePlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: firstSnareMusicFile!))
        }
            
        catch {
            print(error)
        }
   // secondSnareButton
        let secondSnareMusicFile = Bundle.main.path(forResource: "sample snare 2", ofType: type)
        
        do{
            try secondSnarePlayer = AVAudioPlayer(contentsOf: URL (fileURLWithPath: secondSnareMusicFile!))
        }
            
        catch {
            print(error)
        }
    }
    
    

    @IBAction func firstKickButton(_ sender: UIButton) {
        examplePlayer.stop()
        firstKickPlayer.play()
    }
    @IBAction func secondKickButton(_ sender: UIButton) {
        examplePlayer.stop()
        secondKickPlayer.play()
    }
    
    @IBAction func firstSampleButton(_ sender: UIButton) {
        examplePlayer.stop()
        firstSamplePlayer.play()
    }
    
    @IBAction func secondSampleButton(_ sender: UIButton) {
        examplePlayer.stop()
secondSamplePlayer.play()
    }
    @IBAction func firstSnareButton(_ sender: UIButton) {
        examplePlayer.stop()
        firstSnarePlayer.play()
    }
    @IBAction func secondSnareButton(_ sender: UIButton) {
        
        examplePlayer.stop()
        
        secondSnarePlayer.play()
        
        
    }
    
    @IBAction func playMusic(_ sender: UIButton) {
        musicFirstPlayer.prepareToPlay()
        musicReplayPlayer.prepareToPlay()
        musicFirstPlayer.updateMeters()
         musicReplayPlayer.updateMeters()
        musicReplayPlayer.stop()
 examplePlayer.stop()
        musicFirstPlayer.play()
    }
    
    @IBAction func stopMusic(_ sender: UIButton) {
        musicFirstPlayer.prepareToPlay()
        musicReplayPlayer.prepareToPlay()
        examplePlayer.stop()
        musicFirstPlayer.stop()
        musicReplayPlayer.stop()
       
        
    }
    @IBAction func MusicReplayButton(_ sender: UIButton) {
        musicFirstPlayer.prepareToPlay()
        musicReplayPlayer.prepareToPlay()
        musicFirstPlayer.updateMeters()
        musicReplayPlayer.updateMeters()
        examplePlayer.stop()
        musicFirstPlayer.stop()
        musicReplayPlayer.play()
    }

    @IBAction func thirdKickButton(_ sender: UIButton) {
        examplePlayer.stop()
        thirdKickPlayer.play()
    }
    
    @IBAction func fourthKickButton(_ sender: UIButton) {
        examplePlayer.stop()
        fourthKickPlayer.play()
    }
    
    @IBAction func fifthKickButton(_ sender: UIButton) {
        examplePlayer.stop()
        fifthsKickPlayer.play()
    }
    
    @IBAction func hitButton(_ sender: UIButton) {
        examplePlayer.stop()
        hitPlayer.play()
    }
    
    @IBAction func hihatsButton(_ sender: UIButton) {
        examplePlayer.stop()
        hihatsPlayer.play()
    }
    
    @IBAction func fillButton(_ sender: UIButton) {
        examplePlayer.stop()
        fillPlayer.play()
    }
    @IBAction func firstVoxButton(_ sender: UIButton) {
        examplePlayer.stop()
        firstVoxPlayer.play()
    }
    
    @IBAction func secondVoxButton(_ sender: UIButton) {
        examplePlayer.stop()
        secondVoxPlayer.play()
    }
    
    @IBAction func exampleButton(_ sender: UIButton) {
        
        musicReplayPlayer.stop()
        firstKickPlayer.stop()
        secondKickPlayer.stop()
        thirdKickPlayer.stop()
        fourthKickPlayer.stop()
        fifthsKickPlayer.stop()
        hitPlayer.stop()
        hihatsPlayer.stop()
        fillPlayer.stop()
        firstVoxPlayer.stop()
        secondVoxPlayer.stop()
        firstSamplePlayer.stop()
        secondSamplePlayer.stop()
        firstSnarePlayer.stop()
        secondSnarePlayer.stop()
        examplePlayer.play()
    }
    }

