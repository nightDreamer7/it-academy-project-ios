//
//  WeatherViewController.swift
//  Drum Bar
//
//  Created by Alex on 1/3/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   
        
        
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            searchBar.delegate = self
        }
    }
    
    extension ViewController: UISearchBarDelegate {
        
        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            
            let urlString = "https://api.apixu.com/v1/current.json?key=5db0b08c32ed466cb53193644182211&q=\(searchBar.text!.replacingOccurrences(of: " ", with: "%20"))"
            
            var locationName : String?
            var temperature: Double?
            var errorHasOccured: Bool = false
            let url = URL(string: urlString)
            let task = URLSession.shared.dataTask(with: url!) {[weak self] ( data, response , error) in
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: AnyObject]
                    
                    if let _ = json["error"] {
                        errorHasOccured = true
                    }
                    if let location = json["location"]  {
                        locationName = location["name"] as? String
                        
                    }
                    
                    if let current = json["current"] {
                        temperature = current["temp_c"] as? Double
                    }
                    
                    DispatchQueue.main.async {
                        if errorHasOccured {
                            self?.cityLabel.text = " Error has occured"
                            self?.temperatureLabel.isHidden = true
                        } else{
                            self?.cityLabel.text = locationName
                            self?.temperatureLabel.text = "\(temperature!)"
                            
                            self?.temperatureLabel.isHidden = false
                        }
                    }
                    
                    
                }
                catch let jasonError {
                    print(jasonError)
                }
                
            }
            task.resume()
        }
    }

    
    
    
    


        

    

   


