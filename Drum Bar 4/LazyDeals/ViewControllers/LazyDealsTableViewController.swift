//
//  LazyDealsTableViewController.swift
//  Drum Bar
//
//  Created by Alex on 1/4/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class LazyDealsTableViewController: UITableViewController {

    
    var dataDase : [[String : Any]] {
        get {
            let checker =  UserDefaults.standard.array(forKey: "dataArrayKey") as? [[String : Any]]
            if checker == nil {
                
                return []
            } else {
                return checker!
            }
            
            
        } set {
         UserDefaults.standard.set(newValue, forKey: "dataArrayKey")// UserDefaults
           UserDefaults.standard.synchronize()
        }
    }
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelectionDuringEditing = true
    }
    
    @IBAction func DismissBarItem(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func pushEditAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
    @IBAction func barButton(_ sender: UIBarButtonItem) {
      let alertController =   UIAlertController(title: "New Lazy Deal", message: "Are you ready for new Lazy Deal??????", preferredStyle: UIAlertController.Style.alert)
        
        alertController.addTextField(configurationHandler: ({ (textField) in
            textField.placeholder = "Deal Name"
        }))
    
    let alertAdd = UIAlertAction(title: "Add", style: UIAlertAction.Style.default) { (alert) in
        if alertController.textFields?[0].text != "" {
            let newDictionary = ["name": alertController.textFields![0].text!,"isDealDone": false, "color": "white"] as [String : Any]
            
            self.dataDase.append(newDictionary)
           
            
            self.tableView.reloadData()
            
        }
        }
        let alertCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default) { (alert) in
            
        }
    
        alertController.addAction(alertAdd)
    alertController.addAction(alertCancel)
        present(alertController,animated: true, completion: nil)
        
    
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataDase.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyIdettyfire", for: indexPath)

        let itemDictionary = dataDase[indexPath.row]
        
        cell.textLabel?.text = itemDictionary["name"] as? String
        
       let isDone = itemDictionary["isDealDone"] as! Bool
        if isDone {
            cell.accessoryType = .checkmark
        } else {
                cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if !tableView.isEditing {
            
        
        let itemDictionary = dataDase[indexPath.row]
        
         let isDone = itemDictionary["isDealDone"] as! Bool

        if isDone {
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
            dataDase[indexPath.row]["isDealDone"] = false
        } else {
             tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            dataDase[indexPath.row]["isDealDone"] = true
        }
        } else {
            let alertEditController = UIAlertController(title: "Edit The Lazy Deal", message: "Are you still editing without lazy work?????", preferredStyle: UIAlertController.Style.alert)
            
            alertEditController.addTextField { (textField) in
                textField.placeholder = "Lazy Deal Name"
                textField.text = self.dataDase[indexPath.row]["Deal Name"] as? String
               
            }
        
            let alertActionEdit = UIAlertAction(title: "Edit Lazy Deal", style: UIAlertAction.Style.default) { (alert) in
                self.dataDase[indexPath.row]["Deal Name"] = alertEditController.textFields?[0].text
                 self.tableView.reloadData()
            }
        let alertActionCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil)
       
            alertEditController.addAction(alertActionEdit)
            alertEditController.addAction(alertActionCancel)
            
            present(alertEditController, animated: true,completion: nil)
            
        }
        
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
       
        if editingStyle == .delete {
           dataDase.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

        
        let fromItem = dataDase[fromIndexPath.row]
        dataDase.remove(at: fromIndexPath.row)
        dataDase.insert(fromItem, at: to.row)
    }
    

   
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
   

}
