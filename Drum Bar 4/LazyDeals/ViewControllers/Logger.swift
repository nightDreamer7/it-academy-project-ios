//
//  Logger.swift
//  Drum Bar
//
//  Created by Alex on 1/4/19.
//  Copyright © 2019 Alex. All rights reserved.
//


// VC Life Cycle
import UIKit

class VCLogger: UIViewController {
    
    
    private struct LoginGobals {
        
        var prefix = ""
        var instanceCount = Dictionary<String, Int>()
        var lastLoggingDate = Date()
        var intrvalForCreation: TimeInterval = 1
        var separator = "__"
    }
    
    private static var logGlobs = LoginGobals()
    
    static func logPrefix(for className: String) -> String {
        
        if logGlobs.lastLoggingDate.timeIntervalSinceNow < -logGlobs.intrvalForCreation {
            
            logGlobs.prefix += logGlobs.separator
            
        }
        logGlobs.lastLoggingDate = Date()
        
        return logGlobs.prefix + className
    }
    
    private static func instanceCount(for className: String) -> Int {
        
        logGlobs.instanceCount[className] = logGlobs.instanceCount[className] ?? 0 + 1
        
        return logGlobs.instanceCount[className]!
    }
    
    private var instanceCount: Int!
    
    private func logVLC(_ msg: String) {
        
        let className = String(describing: type(of: self))
        
        if instanceCount == nil {
            
            instanceCount = VCLogger.instanceCount(for: className)
            
            
        }
        
        print("\(VCLogger.logPrefix(for: className))(\(instanceCount!))\(msg)")
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        logVLC("init(coder:) - Initiated view IB")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        logVLC("init(coder:) - Initiated view IB")
    }
    deinit {
        logVLC("leaved the stack")
        
    }
    override func awakeFromNib() {
        
        logVLC("Awake from NIB Method")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logVLC("ViewDid load Method")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        logVLC("ViewWillAppear Method")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        logVLC("ViewWillAppear Method")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        logVLC("ViewWillLayputSubviews Method")
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        logVLC("ViewDidLayputSubviews Method")
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        logVLC("ViewWillDisappear Method")
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        logVLC("ViewDidDisappear Method")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        logVLC("ViewWillTransition Method")
    }
}
