//
//  LazyDrawer.swift
//  Drum Bar
//
//  Created by Alex on 1/4/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit


class LazyDrawer: UIView {
    var animationTime: TimeInterval = 0.7
    
   var scale: CGFloat = 0.95
    
    private var height: CGFloat{
        
        return min(bounds.size.width, bounds.size.height)
    }
    
    private var headPhonesRadius: CGFloat{
        
        return height / 7 * scale
}
    private var headCircleRadius: CGFloat {
        return height / 5 * scale
    }
    
    private var headCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    private var leftHeadCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX - headCircleRadius, y: bounds.midY)
    }
    private var rightHeadCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX + headCircleRadius, y: bounds.midY)
    }
    private var firstRightLineCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX + headCircleRadius, y: bounds.midY - headPhonesRadius)
    }
    private var secondRightLineCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX + headCircleRadius, y: bounds.midY + headPhonesRadius)
    }
    private var firstLefrLineCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX - headCircleRadius, y: bounds.midY - headPhonesRadius)
    }
    private var secondLefrLineCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX - headCircleRadius, y: bounds.midY + headPhonesRadius)
    }
    private func pathForFirstCircle() -> UIBezierPath{
        let headCirclePath = UIBezierPath(arcCenter: headCircleCenter, radius: headCircleRadius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        headCirclePath.lineWidth = 5.0
        return headCirclePath
    }
    private func pathForLeftHedphoneCircle() -> UIBezierPath{
        let headphoneLeftCirclePath = UIBezierPath(arcCenter: leftHeadCircleCenter, radius: headPhonesRadius, startAngle: CGFloat.pi / 2, endAngle: 1.5 * CGFloat.pi, clockwise: true)
        headphoneLeftCirclePath.lineWidth = 5.0
        return headphoneLeftCirclePath
    }
    private func pathForRightHedphoneCircle() -> UIBezierPath{
        let headphoneRightCirclePath = UIBezierPath(arcCenter: rightHeadCircleCenter, radius: headPhonesRadius, startAngle: CGFloat.pi / 2, endAngle: 1.5 * CGFloat.pi, clockwise: false)
        headphoneRightCirclePath.lineWidth = 5.0
        return headphoneRightCirclePath
    }
    private func pathForRightLineHedphoneCircle() -> UIBezierPath{
        let headphoneLineRightCirclePath = UIBezierPath()
        headphoneLineRightCirclePath.move(to: firstRightLineCircleCenter)
        headphoneLineRightCirclePath.addLine(to: secondRightLineCircleCenter)
        headphoneLineRightCirclePath.lineWidth = 5.0
        return headphoneLineRightCirclePath
    }
    private func pathForLeftLineHedphoneCircle() -> UIBezierPath{
        let headphoneLineLeftCirclePath = UIBezierPath()
        headphoneLineLeftCirclePath.move(to: firstLefrLineCircleCenter)
         headphoneLineLeftCirclePath.addLine(to: secondLefrLineCircleCenter)
        headphoneLineLeftCirclePath.lineWidth = 5.0
        return headphoneLineLeftCirclePath
    }
    override func draw(_ rect: CGRect) {
        UIColor.black.set()
        pathForFirstCircle().stroke()
        pathForLeftHedphoneCircle().stroke()
        pathForLeftLineHedphoneCircle().stroke()
        pathForRightLineHedphoneCircle().stroke()
        pathForRightHedphoneCircle().stroke()
    }
}
