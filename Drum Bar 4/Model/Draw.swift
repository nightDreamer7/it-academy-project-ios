//
//  Draw.swift
//  Drum Bar
//
//  Created by Alex on 1/4/19.
//  Copyright © 2019 Alex. All rights reserved.
//


import UIKit


class Snowman: UIView{
    
    var scale: CGFloat = 0.95
    
    private var height: CGFloat{
        
        return min(bounds.size.width, bounds.size.height)
    }
    
    private var firstCircleRadius: CGFloat{
        
        return height / 5 * scale
    }
    
    private var secondCircleRadius: CGFloat {
        return height / 7 * scale
    }
    
    private var headCircleRadius: CGFloat{
        return height / 10 * scale
    }
    private var leftEyeCenter: CGPoint {
        return CGPoint(x: bounds.midX - 9, y: bounds.midY - 2 * secondCircleRadius - 7)
    }
    private var smileCenter: CGPoint {
        return CGPoint(x: bounds.midX , y: bounds.midY - 2 * secondCircleRadius + 14)
    }
    private var rightEyeCenter: CGPoint {
        return CGPoint(x: bounds.midX + 9, y: bounds.midY - 2 * secondCircleRadius - 7)
    }
    
    
    ////// arms //////
    private var leftArmCenter: CGPoint {
        return CGPoint(x: bounds.midX - secondCircleRadius, y: bounds.midY + headCircleRadius - secondCircleRadius)
    }
    private var leftArmSecondPoint: CGPoint {
        return CGPoint(x: bounds.midX - 2 * secondCircleRadius, y: bounds.midY + headCircleRadius - 1.3 * secondCircleRadius)
    }
    private var leftArmThirdPoint: CGPoint {
        return CGPoint(x: bounds.midX - 2.1 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.2 * secondCircleRadius)
    }
    private var leftArmFirstFinger: CGPoint {
        return CGPoint(x: bounds.midX - 2.4 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.4 * secondCircleRadius)
    }
    private var leftArmSecondFinger: CGPoint {
        return CGPoint(x: bounds.midX - 2.1 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.2 * secondCircleRadius)
    }
    private var leftArmThirdFinger: CGPoint {
        return CGPoint(x: bounds.midX - 1.7 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.4 * secondCircleRadius)
    }
    private var leftLegCenter: CGPoint {
        return CGPoint(x: bounds.midX -   firstCircleRadius, y: bounds.midY + headCircleRadius + 1.9 * firstCircleRadius )
        
    }
    private var leftLegSize: CGSize {
        return CGSize(width: 0.55 * firstCircleRadius, height: 0.35 * firstCircleRadius)
        
    }
    private var leftLegRect: CGRect {
        return CGRect(origin: leftLegCenter, size: leftLegSize)
        
    }
    
    /////////////////////////////
    private var rightArmCenter: CGPoint {
        return CGPoint(x: bounds.midX + secondCircleRadius, y: bounds.midY + headCircleRadius - secondCircleRadius)
    }
    private var rightArmSecondPoint: CGPoint {
        return CGPoint(x: bounds.midX + 2 * secondCircleRadius, y: bounds.midY + headCircleRadius - 1.3 * secondCircleRadius)
    }
    private var rightArmThirdPoint: CGPoint {
        return CGPoint(x: bounds.midX + 2.1 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.2 * secondCircleRadius)
    }
    private var rightArmFirstFinger: CGPoint {
        return CGPoint(x: bounds.midX + 2.4 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.4 * secondCircleRadius)
    }
    private var rightArmSecondFinger: CGPoint {
        return CGPoint(x: bounds.midX + 2.1 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.2 * secondCircleRadius)
    }
    private var rightArmThirdFinger: CGPoint {
        return CGPoint(x: bounds.midX + 1.7 * secondCircleRadius, y: bounds.midY + headCircleRadius - 2.4 * secondCircleRadius)
    }
    private var rightLegCenter: CGPoint {
        return CGPoint(x: bounds.midX + 0.6 * firstCircleRadius, y: bounds.midY + headCircleRadius + 1.9 * firstCircleRadius )
        
    }
    private var rightLegSize: CGSize {
        return CGSize(width: 0.55 * firstCircleRadius, height: 0.35 * firstCircleRadius)
        
    }
    private var rightLegRect: CGRect {
        return CGRect(origin: rightLegCenter, size: rightLegSize)
        
    }
    /////////////////
    private var firstCircleCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY + headCircleRadius + firstCircleRadius)
        
    }
    private var secondCircleCenter: CGPoint{
        return CGPoint(x: bounds.midX, y: bounds.midY + headCircleRadius - secondCircleRadius)
    }
    
    private var headCenter: CGPoint {
        return CGPoint(x: bounds.midX, y: bounds.midY - 2 * secondCircleRadius)
    }
    
    private func pathForFirstCircle() -> UIBezierPath{
        let firstCirclePath = UIBezierPath(arcCenter: firstCircleCenter, radius: firstCircleRadius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        firstCirclePath.lineWidth = 5.0
        return firstCirclePath
    }
    ///////////////////////
    private func pathForLeftLeg() -> UIBezierPath{
        let leftLegPath = UIBezierPath(ovalIn: leftLegRect)
        leftLegPath.lineWidth = 5.0
        return leftLegPath
    }
    private func pathForRightLeg() -> UIBezierPath{
        let rightLegPath = UIBezierPath(ovalIn: rightLegRect)
        rightLegPath.lineWidth = 5.0
        return rightLegPath
    }
    private func pathForSecondCircle() -> UIBezierPath {
        let secondCirclePath = UIBezierPath(arcCenter: secondCircleCenter, radius: secondCircleRadius, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
        secondCirclePath.lineWidth = 5.0
        return secondCirclePath
    }
    private func pathForleftArm() -> UIBezierPath {
        let leftArmPath = UIBezierPath()
        leftArmPath.move(to: leftArmCenter)
        leftArmPath.addLine(to: leftArmSecondPoint)
        leftArmPath.addLine(to: leftArmThirdPoint)
        leftArmPath.addLine(to: leftArmFirstFinger)
        leftArmPath.move(to:leftArmThirdPoint )
        leftArmPath.addLine(to: leftArmSecondFinger)
        leftArmPath.lineWidth = 5.0
        return leftArmPath
    }
    private func pathForleftFinger() -> UIBezierPath {
        let leftFingerPath = UIBezierPath()
        
        leftFingerPath.move(to:leftArmThirdPoint )
        leftFingerPath.addLine(to: leftArmThirdFinger)
        leftFingerPath.lineWidth = 5.0
        return leftFingerPath
    }
    private func pathForRightArm() -> UIBezierPath {
        let rightArmPath = UIBezierPath()
        rightArmPath.move(to: rightArmCenter)
        rightArmPath.addLine(to: rightArmSecondPoint)
        rightArmPath.addLine(to: rightArmThirdPoint)
        rightArmPath.addLine(to: rightArmFirstFinger)
        rightArmPath.move(to:rightArmThirdPoint )
        rightArmPath.addLine(to: rightArmSecondFinger)
        rightArmPath.lineWidth = 5.0
        return rightArmPath
    }
    private func pathForRightFinger() -> UIBezierPath {
        let rightFingerPath = UIBezierPath()
        
        rightFingerPath.move(to:rightArmThirdPoint )
        rightFingerPath.addLine(to: rightArmThirdFinger)
        rightFingerPath.lineWidth = 5.0
        return rightFingerPath
    }
    private func pathForHead() -> UIBezierPath{
        let headPath = UIBezierPath(arcCenter: headCenter, radius: headCircleRadius, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
        headPath.lineWidth = 5.0
        return headPath
    }
    private func pathForSmile() -> UIBezierPath{
        let smilePath = UIBezierPath(arcCenter: smileCenter, radius: 10, startAngle: 0, endAngle: CGFloat.pi, clockwise: true)
        smilePath.lineWidth = 5.0
        return smilePath
    }
    private func pathForLeftEye() -> UIBezierPath{
        let leftEyePath = UIBezierPath(arcCenter: leftEyeCenter, radius: 2, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
        leftEyePath.lineWidth = 5.0
        return leftEyePath
    }
    
    private func pathForRightEye() -> UIBezierPath{
        let rightEyePath = UIBezierPath(arcCenter: rightEyeCenter, radius: 2, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
        rightEyePath.lineWidth = 5.0
        return rightEyePath
    }
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        UIColor.black.set()
        pathForFirstCircle().stroke()
        pathForSecondCircle().stroke()
        pathForHead().stroke()
        pathForleftArm().stroke()
        pathForleftFinger().stroke()
        pathForSmile().stroke()
        pathForRightArm().stroke()
        pathForRightFinger().stroke()
        pathForLeftLeg().stroke()
        pathForRightLeg().stroke()
        
        
        UIColor.red.setStroke()
        
        
        pathForLeftEye().stroke()
        pathForRightEye().stroke()
    }
    private struct Constants {
        static let eyesOffset = 3
        
    
    
}


}
